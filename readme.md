Drugi domaci zadatak.
Trebalo je uraditi sledece:
Kreirati stranicu koja poseduje jedan obrazac ali sa min dva izdvojena dela.
Obavezno treba da se u obrascu koriste pored tekstualnih polja radio tasteri,
checkboxovi, select liste sa grupisanjem, tasteri. Ispod obrasca ubaciti u kontener
video i audio.
Header sa tri linka koji skacu na tri razlicite sekcije stranice.
Koristiti meta tagove. Strukturu stranice uraditi preko kontejnerskih elemenata.
Poseban naglasak obratiti na pravilno definisanje atributa kod svih polja (ime,
vrednosti) kao i na zaglavlje obrasca.
Podaci se šalju POST metodom na stranicu register.php